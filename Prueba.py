import os
import sqlite3

def CrearDB():
	conexion = sqlite3.connect('Ejemplo.db')
	cursor = conexion.cursor()

	cursor.execute("CREATE TABLE IF NOT EXISTS Pj (name TEXT PRIMARY KEY, lvl INTEGER, exp INTEGER, gold INTEGER)")
	conexion.commit()
	
	cursor.close()
	conexion.close()

def GuardarDatos(name_, lvl_, exp_, gold_):
	conexion = sqlite3.connect('Ejemplo.db')
	cursor = conexion.cursor()

	try:
		cursor.execute("INSERT INTO Pj(name, lvl, exp, gold) VALUES (?,?,?,?)", (name_, lvl_, exp_, gold_))
	except Exception:
		cursor.execute("UPDATE Pj SET lvl = ? WHERE name = ?", (lvl_, name_))
		cursor.execute("UPDATE Pj SET exp = ? WHERE name = ?", (exp_, name_))
		cursor.execute("UPDATE Pj SET gold = ? WHERE name = ?", (gold_, name_))
		
	conexion.commit()
	cursor.close()
	conexion.close()

def Inicio():
	conexion = sqlite3.connect('Ejemplo.db')
	cursor = conexion.cursor()
	a = cursor.execute("SELECT count(*) FROM Pj").fetchone()[0]
	cursor.close()
	conexion.close()
	cp = True

	print("Bienvenido:")
	print("1 -> Iniciar partida")
	if a!=0:
		print("2 -> Cargar partida")
		cp = False
	else:
		cp = True
	emp = input()

	if emp == '1':
		name = input("¿Como te llamas?")
		return (name, 0, 0, 0)
	elif emp == '2' and not cp:
		datos = Cargar()
		return datos
	else:
		Inicio()


def Cargar():
	conexion = sqlite3.connect('Ejemplo.db')
	cursor = conexion.cursor()

	a = cursor.execute("SELECT count(*) FROM Pj").fetchone()[0]
	
	if a==0:
		conexion.commit()
		cursor.close()
		conexion.close()
		print("No hay partidas guardadas")
		return None
	else:
		for row in cursor.execute("SELECT name, lvl, exp, gold from Pj"):
			print("Name = ", row[0])
			print("Lvl = ", row[1])
			print("Exp = ", row[2])
			print("Gold = ", row[3], "\n")

		carg = input("Escribe el nombre de la partida a cargar: ")

		name = carg
		lvl = cursor.execute("SELECT lvl from Pj where name=?",(carg,)).fetchone()
		exp = cursor.execute("SELECT exp from Pj where name=?",(carg,)).fetchone()
		gold = cursor.execute("SELECT gold from Pj where name=?",(carg,)).fetchone()

		lvl = lvl[0]
		exp = exp[0]
		gold = gold[0]

		conexion.commit()
		cursor.close()
		conexion.close()

		array = (name, lvl, exp, gold)
		return array
#----------------------------------

CrearDB()
Datos = Inicio()

name = Datos[0]
lvl = Datos[1]
exp = Datos[2]
gold = Datos[3]

while True:
	print("""
1 -> Experiencia
2 -> Oro
3 -> Ver Datos
4 -> Guardar Datos""")
	accion = input()

	if accion=="1":
		exp+=10
	elif accion=="2":
		gold+=5;
	elif accion=="3":
		print("""
Nombre: {}
lvl: {}
Exp: {}
Oro: {}""".format(name, lvl, exp, gold))
	elif accion=="4":
		GuardarDatos(name, lvl, exp, gold)

os.system("pause")